package com.example.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xushun
 * @email 505232797@qq.com
 * @date 2019/4/23 16:35
 */
@SpringBootApplication
@EnableDiscoveryClient
@RestController
public class ProviderApplication {
    public static void main(String[] args) {
        SpringApplication.run(ProviderApplication.class, args);
    }

    @GetMapping("hello/{name}")
    public String hello(@PathVariable String name){
        return "hello " + name;
    }
}
