package com.example.consumer;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author xushun
 * @email 505232797@qq.com
 * @date 2019/4/23 16:41
 */
@FeignClient(name = "provider")
public interface ProviderService {
    @GetMapping("hello/{name}")
    String hello(@PathVariable String name);
}
