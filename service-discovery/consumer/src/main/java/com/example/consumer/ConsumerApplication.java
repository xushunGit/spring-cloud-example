package com.example.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xushun
 * @email 505232797@qq.com
 * @date 2019/4/23 16:40
 */
@SpringBootApplication
@EnableFeignClients
@EnableDiscoveryClient
@RestController
public class ConsumerApplication {
    public static void main(String[] args) {
        SpringApplication.run(ConsumerApplication.class, args);
    }

    @Autowired
    private ProviderService providerService;

    @GetMapping("hello/{name}")
    public String hello(@PathVariable String name) {
        return providerService.hello(name + " (from consumer)");
    }
}
